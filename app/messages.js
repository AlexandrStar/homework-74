const express = require('express');
const fileDb = require('../fileDb');

const router = express.Router();

router.get('/', (req, res) => {
  res.send(fileDb.getItems());
});

router.post('/', (req, res) => {
  fileDb.addItem(req.body);
  res.send({message: 'OK'});
});

module.exports = router;