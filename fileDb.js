const fs = require('fs');

const path = './messages';

let data = [];

module.exports = {
  init() {
    try {
      const fileContents = fs.readdirSync(path);
      data = fileContents.map(file => {
        return  JSON.parse(fs.readFileSync(`${path}/${file}`));
      })
    } catch (e) {
      data = [];
    }
  },
  getItems() {
    return data.splice(data.length -5);
  },
  addItem(item) {
    const date = new Date().toISOString();
    item.date = date;
    data.push(item);
    this.save(item);
  },
  save(item) {
    const filename = `${path}/${item.date}.txt`;
    fs.writeFileSync(filename, JSON.stringify(item));
  }
};
